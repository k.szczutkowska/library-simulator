package pl.edu.agh.qa.library;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pl.edu.agh.qa.library.items.Book;
import pl.edu.agh.qa.library.items.Item;
import pl.edu.agh.qa.library.items.ItemCounter;
import pl.edu.agh.qa.library.items.Magazine;
import pl.edu.agh.qa.library.users.Lecturer;
import pl.edu.agh.qa.library.users.Student;
import pl.edu.agh.qa.library.users.User;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class LibraryTest {

    @Test(dataProvider = "valid users")
    public void shouldAddUserToLibrary(User... users) {
        Library library = new Library();
        library.addUserToLibrary(users);
        int expectedSizeOfSetWithUniqueIds = 4;
        Set<String> actualIds = library.getLibraryUsers().stream().map(user -> user.getId()).collect(Collectors.toSet());

        Assert.assertEquals(actualIds.size(), expectedSizeOfSetWithUniqueIds);
    }

    @DataProvider(name = "valid users")
    public Object[][] methodProvidingValidUsers() {
        return new Object[][]{
                {new Student("Tom", "DaCat"),
                        new Student("Tom", "DaCat"),
                        new Student("Jerry", "Small"),
                        new Lecturer("Jerry", "Small")},
                {new Student("Xi", "Wi"),
                        new Student("Tom", "DaCat"),
                        new Student("Jerry", "Small"),
                        new Lecturer("Xi", "Wi")}
        };
    }

    @Test(dataProvider = "invalid users")
    public void shouldNotAddUserToLibrary(User user, String invalidReason) {
        Library library = new Library();
        library.addUserToLibrary(user);
        Set<User> actualUsers = library.getLibraryUsers();

        assertThat(actualUsers).doesNotContain(user);
    }

    @DataProvider(name = "invalid users")
    public Object[][] methodProvidingInvalidUsers() {
        return new Object[][]{
                {new Student("", ""), "empty name and empty surname"},
                {new Lecturer("A", "B"), "too short name and too short surname"},
                {new Student("", "OK"), "empty name"},
                {new Student("OK", ""), "empty surname"},
                {new Lecturer("A", "OK"), "too short name"},
                {new Lecturer("OK", "B"), "too short surname"}
        };
    }

    @Test(dataProvider = "valid items")
    public void shouldAddItemToLibrary(int listSize, int quantity, Item... items) {
        Library library = new Library();
        library.addItemToLibrary(items);
        Map<Item, ItemCounter> actualMap = library.getLibraryItems();
        int actualQuantity = actualMap.get(items[0]).getQuantityInTotal();

        Assert.assertEquals(actualMap.size(), listSize);
        Assert.assertEquals(actualQuantity, quantity);
    }

    @DataProvider(name = "valid items")
    public Object[][] methodProvidingValidItems() {
        return new Object[][]{
                {1, 1, new Book("Mario Puzo", "The Godfather")},
                {1, 2, new Magazine("ok", "1"), new Magazine("ok", "1")},
                {3, 1, new Book("Brandon Sanderson", "The Way of Kings"),
                        new Book("Brandon Sanderson", "Words of Radiance"),
                        new Book("Brandon Sanderson", "Oathbringer")},
        };
    }
    @Test(dataProvider = "invalid items")
    public void shouldNotAddItemToLibrary(Item item){
        Library library = new Library();
        library.addItemToLibrary(item);
        Map<Item, ItemCounter> actualMap = library.getLibraryItems();

        Assert.assertEquals(actualMap.size(), 0);
    }

    @DataProvider(name = "invalid items")
    public Object[][] methodProvidingInvalidItems() {
        return new Object[][]{
                {new Book("", "")},
                {new Book("1", "")},
                {new Book("", "1")},
                {new Magazine("", "")},
                {new Magazine("a", "")},
                {new Magazine("", "a")}
        };
    }
}
