package pl.edu.agh.qa.library.items;

import java.util.Objects;

public class Magazine extends Item {
    private final String number;

    public Magazine(String number, String title) {
        super(title);
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    @Override
    public boolean isDataValid() {
        return getTitle().length() > 0 && getNumber().length() > 0;
    }

    @Override
    public String dataToPrint() {
        return getTitle() + "-" + getNumber();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Magazine magazine)) return false;
        if (!super.equals(o)) return false;
        return getNumber().equals(magazine.getNumber());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getNumber());
    }

    @Override
    public String toString() {
        return getTitle() + ";" + number;
    }
}
