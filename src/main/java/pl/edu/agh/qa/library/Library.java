package pl.edu.agh.qa.library;

import pl.edu.agh.qa.library.items.Book;
import pl.edu.agh.qa.library.items.Item;
import pl.edu.agh.qa.library.items.ItemCounter;
import pl.edu.agh.qa.library.items.Magazine;
import pl.edu.agh.qa.library.users.User;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Library {
    private static final String BOOK_MARKER_IN_FILE = "B";
    private static final String MAGAZINE_MARKER_IN_FILE = "M";
    private static final int ITEM_TITLE_POSITION_IN_FILE = 0;
    private static final int ITEM_AUTHOR_OR_NUMBER_POSITION_IN_FILE = 1;
    private static final int ITEM_QUANTITY_POSITION_IN_FILE = 2;
    private static final int ITEM_TYPE_POSITION_IN_FILE = 3;
    private static final String ITEM_SEPARATOR = ";";
    private Map<Item, ItemCounter> libraryItems = new HashMap<>();
    private Map<User, List<Item>> usersRentedItems = new HashMap<>();

    public void addUserToLibrary(User... users) {
        for (User user : users) {
            if (user.isDataValid()) {
                usersRentedItems.put(user, user.getUserItemsList());
            }
        }
    }

    public void addItemToLibrary(Item... items) {
        for (Item item : items) {
            if (libraryItems.containsKey(item)) {
                addExistingItem(item);
            } else {
                if (item.isDataValid()) {
                    addNewItem(item);
                }
            }
        }
    }

    public boolean rentItemToUser(Item item, User user) {
        if (canUserRentItem(user, item)) {
            ItemCounter itemCounter = libraryItems.get(item);
            libraryItems.put(item, new ItemCounter(itemCounter.getQuantityInTotal(),
                    itemCounter.getQuantityToRent() - 1));
            user.updateRentedData(item);
            usersRentedItems.put(user, user.getUserItemsList());
            return true;
        } else {
            return false;
        }
    }

    public String rentedItemsAsString() {
        StringBuilder rented = new StringBuilder();
        for (Map.Entry<User, List<Item>> entry : usersRentedItems.entrySet()) {
            if (!(entry.getValue().isEmpty())) {
                rented.append(entry.getKey().getId()).append("[");
                List<String> itemsList = new ArrayList<>();
                for (int i = 0; i < entry.getValue().size(); i++) {
                    Item item = entry.getValue().get(i);
                    String data = item.dataToPrint();
                    itemsList.add(data);
                }
                String itemsAsString = itemsList.stream().collect(Collectors.joining("; "));
                rented.append(itemsAsString);
                rented.append("]\n");
            }
        }
        return rented.toString();
    }

    public void printListOfMagazines() {
        for (Map.Entry<Item, ItemCounter> entry : libraryItems.entrySet()) {
            if (entry.getKey() instanceof Magazine) {
                System.out.println(entry.getKey() + ITEM_SEPARATOR + entry.getValue().getQuantityInTotal() + ITEM_SEPARATOR +
                        entry.getValue().getQuantityToRent());
            }
        }
    }

    public void printListOfBooks() {
        for (Map.Entry<Item, ItemCounter> entry : libraryItems.entrySet()) {
            if (entry.getKey() instanceof Book) {
                System.out.println(entry.getKey() + ITEM_SEPARATOR + entry.getValue().getQuantityInTotal() + ITEM_SEPARATOR +
                        entry.getValue().getQuantityToRent());
            }
        }
    }

    public void printListOfUsers() {
        for (Map.Entry<User, List<Item>> entry : usersRentedItems.entrySet()) {
            if (entry.getKey().isActive()) {
                System.out.println(entry.getKey().toString());
            }
        }
    }

    public void importItemsFromFile(String csvFile) {
        List<List<String>> records = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(ITEM_SEPARATOR);
                records.add(Arrays.asList(values));
                int quantity = Integer.parseInt(values[ITEM_QUANTITY_POSITION_IN_FILE]);
                if (values[ITEM_TYPE_POSITION_IN_FILE].equals(BOOK_MARKER_IN_FILE)) {
                    Item item = new Book(values[ITEM_AUTHOR_OR_NUMBER_POSITION_IN_FILE], values[ITEM_TITLE_POSITION_IN_FILE]);
                    addItemsToLibrary(item, quantity);
                } else if (values[ITEM_TYPE_POSITION_IN_FILE].equals(MAGAZINE_MARKER_IN_FILE)) {
                    Item item = new Magazine(values[ITEM_AUTHOR_OR_NUMBER_POSITION_IN_FILE], values[ITEM_TITLE_POSITION_IN_FILE]);
                    addItemsToLibrary(item, quantity);
                }
            }
        } catch (
                IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void exportUsersWithItemsToFile(String filePath) {
        try (FileWriter fileWriter = new FileWriter(filePath)) {
            fileWriter.write(rentedItemsAsString());
        } catch (IOException e) {
            System.out.println("Unable to write to file");
        }
    }

    public void printListOfAllUsers() {
        for (Map.Entry<User, List<Item>> entry : usersRentedItems.entrySet()) {
            if (entry.getKey().isActive()) {
                System.out.println(entry.getKey().toString());
            } else {
                System.out.println(entry.getKey().toString() + " USER NOT ACTIVE");
            }
        }
    }

    public Set<User> getLibraryUsers() {
        Set<User> libraryUsers = new HashSet<>();
        for (Map.Entry<User, List<Item>> entry : usersRentedItems.entrySet()) {
            libraryUsers.add(entry.getKey());
        }
        return libraryUsers;
    }

    public Map<Item, ItemCounter> getLibraryItems() {
        return libraryItems;
    }

    private void addItemsToLibrary(Item item, int quantity) {
        for (int i = 1; i <= quantity; i++) {
            if (libraryItems.containsKey(item)) {
                addExistingItem(item);
            } else {
                addNewItem(item);
            }
        }
    }

    private void addExistingItem(Item item) {
        ItemCounter itemCounter = libraryItems.get(item);
        libraryItems.put(item, new ItemCounter(itemCounter.getQuantityInTotal() + 1,
                itemCounter.getQuantityToRent() + 1));
    }

    private void addNewItem(Item item) {
        libraryItems.put(item, new ItemCounter(1, 1));
    }

    private boolean canUserRentItem(User user, Item item) {
        if (libraryItems.containsKey(item)) {
            return (libraryItems.get(item).getQuantityToRent() > 0) && user.canRent();
        } else {
            return false;
        }
    }
}