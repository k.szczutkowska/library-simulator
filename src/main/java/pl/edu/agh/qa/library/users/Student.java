package pl.edu.agh.qa.library.users;

public class Student extends User {

    private static final int STUDENT_RENT_LIMIT = 4;

    public Student(String firstName, String surname) {
        super(firstName, surname);
        setAccountType("S");
        setLimit(STUDENT_RENT_LIMIT);
    }
}


