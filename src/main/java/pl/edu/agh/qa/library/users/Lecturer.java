package pl.edu.agh.qa.library.users;

public class Lecturer extends User {

    private static final int LECTURER_RENT_LIMIT = 10;

    public Lecturer(String firstName, String surname) {
        super(firstName, surname);
        setAccountType("L");
        setLimit(LECTURER_RENT_LIMIT);
    }
}
