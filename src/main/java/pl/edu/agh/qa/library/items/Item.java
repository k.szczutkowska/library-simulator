package pl.edu.agh.qa.library.items;

import java.util.Objects;

public abstract class Item {
    private final String title;

    public Item(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public abstract String dataToPrint();

    public abstract boolean isDataValid();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Item item)) return false;
        return getTitle().equals(item.getTitle());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTitle());
    }

    @Override
    public abstract String toString();
}
