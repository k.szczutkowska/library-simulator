package pl.edu.agh.qa.library.items;

public class ItemCounter {
    private final int quantityInTotal;
    private final int quantityToRent;

    public ItemCounter(int quantityInTotal, int quantityToRent) {
        this.quantityInTotal = quantityInTotal;
        this.quantityToRent = quantityToRent;
    }

    public int getQuantityInTotal() {
        return quantityInTotal;
    }

    public int getQuantityToRent() {
        return quantityToRent;
    }
}
