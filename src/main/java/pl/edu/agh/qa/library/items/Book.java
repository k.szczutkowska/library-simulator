package pl.edu.agh.qa.library.items;

import java.util.Objects;

public class Book extends Item {
    private final String author;

    public Book(String author, String title) {
        super(title);
        this.author = author;
    }

    public String getAuthor() {
        return author;
    }

    @Override
    public boolean isDataValid() {
        return getTitle().length() > 0 && getAuthor().length() > 0;
    }

    @Override
    public String dataToPrint() {
        return getTitle() + "-" + getAuthor();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book book)) return false;
        if (!super.equals(o)) return false;
        return getAuthor().equals(book.getAuthor());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getAuthor());
    }

    @Override
    public String toString() {
        return getTitle() + ";" + author;
    }
}

