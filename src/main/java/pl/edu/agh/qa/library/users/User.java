package pl.edu.agh.qa.library.users;

import pl.edu.agh.qa.library.items.Item;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public abstract class User {
    private static int nextID = 1;
    private String id;
    private final String firstName;
    private final String surname;
    private String accountType;
    private int limit;
    private int rented;
    private List<Item> userItemsList = new ArrayList<>();
    private boolean active = true;


    public User(String firstName, String surname) {
        this.firstName = firstName;
        this.surname = surname;
        setId();
    }

    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSurname() {
        return surname;
    }

    public String getAccountType() {
        return accountType;
    }

    public int getRented() {
        return rented;
    }

    public int getLimit() {
        return limit;
    }

    public List<Item> getUserItemsList() {
        return userItemsList;
    }

    public boolean isActive() {
        return active;
    }

    public void setRented(int rented) {
        this.rented = rented;
    }

    public void setUserItemsList(List<Item> userItemsList) {
        this.userItemsList = userItemsList;
    }

    protected void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    protected void setId() {
        id = "ID" + nextID;
        nextID++;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean canRent() {
        return getLimit() > getRented() && isActive();
    }

    public void updateRentedData(Item item) {
        List<Item> listOfItems = getUserItemsList();
        listOfItems.add(item);
        setUserItemsList(listOfItems);
        setRented(getRented() + 1);
    }

    public boolean isDataValid() {
        return getFirstName().length() >= 2 && ((getSurname().length() >= 2));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User user)) return false;
        return Objects.equals(getId(), user.getId()) && Objects.equals(getFirstName(), user.getFirstName()) && Objects.equals(getSurname(), user.getSurname());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFirstName(), getSurname());
    }

    @Override
    public String toString() {
        return getFirstName() + ";" + getSurname() + ";" + getId() + ";" + getAccountType();
    }
}
