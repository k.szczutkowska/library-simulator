package pl.edu.agh.qa.library;

import pl.edu.agh.qa.library.items.Book;
import pl.edu.agh.qa.library.items.Item;
import pl.edu.agh.qa.library.items.Magazine;
import pl.edu.agh.qa.library.users.Lecturer;
import pl.edu.agh.qa.library.users.Student;
import pl.edu.agh.qa.library.users.User;

public class LibraryRunner {

    public static void main(String[] args) {
        Library myLibrary = new Library();

        Item i1 = new Book("Brandon Sanderson", "The Way of Kings");
        Item i2 = new Book("Brandon Sanderson", "Words of Radiance");
        Item i3 = new Book("Brandon Sanderson", "Oathbringer");
        Item i4 = new Book("Jane Austen", "Pride and prejudice");
        Item i5 = new Book("Mario Puzo", "Omerta");
        Item i6 = new Book("Mario Puzo", "The Godfather");
        Item i16 = new Book("Mario Puzo", "The Godfather");

        Item i7 = new Magazine("01/2020", "National Geografic");
        Item i8 = new Magazine("02/2020", "National Geografic");
        Item i9 = new Magazine("03/2020", "National Geografic");
        Item i10 = new Magazine("01/2020", "The Scientist");
        Item i11 = new Magazine("02/2020", "The Scientist");
        Item i12 = new Magazine("03/2020", "The Scientist");
        Item i13 = new Magazine("04/2020", "The Scientist");
        Item i14 = new Magazine("04/2020", "The Scientist");
        Item i15 = new Magazine("04/2023", "The Scientist");

        User u1 = new Student("Tom", "DaCat");
        User u2 = new Student("Jerry", "Small");
        User u3 = new Lecturer("Tom", "Bond");
        User u4 = new Student("Jane", "Boom");
        User u5 = new Student("Dan", "Turtle");
        User u6 = new Student("Bob", "Dudley");
        User u7 = new Lecturer("Anna", "Left");
        User u8 = new Lecturer("Anna", "Left");
        User u9 = new Lecturer("Jenny", "Long");

        myLibrary.addItemToLibrary(i1, i1, i1, i1, i1, i2, i3, i4, i5, i6, i16);
        myLibrary.addItemToLibrary(i7, i8, i9, i10, i11, i12, i13, i14);
        myLibrary.addUserToLibrary(u1, u2, u3, u4, u5, u6, u7, u8, u9);
        myLibrary.importItemsFromFile("C:\\Users\\Karolina\\Desktop\\itemsToImport.csv");
        myLibrary.printListOfBooks();
        myLibrary.rentItemToUser(i15, u3);
        myLibrary.rentItemToUser(i1, u3);
        myLibrary.rentItemToUser(i7, u3);
        myLibrary.rentItemToUser(i2, u1);
        System.out.println(myLibrary.rentedItemsAsString());
        myLibrary.exportUsersWithItemsToFile("C:\\Users\\Karolina\\Desktop\\items2.csv");
        Item mamaMu = new Book("Sven Nordqvist, Jujja Wieslander", "Mama Mu nabija sobie guza");
        myLibrary.printListOfMagazines();
        myLibrary.printListOfUsers();
        u2.setActive(false);
        myLibrary.printListOfAllUsers();
        System.out.println(myLibrary.rentedItemsAsString());
    }
}
